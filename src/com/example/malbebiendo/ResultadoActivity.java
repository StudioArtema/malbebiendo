package com.example.malbebiendo;

import java.io.IOException;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;



public class ResultadoActivity extends Activity {
	private TextView txtResultado;
	private SQLiteDatabase db;
	private Typeface futuraHandwritten;
	public static int cont1=0;
	public static int cont2=0;
	public static int cont3=0;
	public static int cont4=0;
	public static int cont5=0;
	public static boolean[] repetidos1 = new boolean[25];
	public static boolean[] repetidos2 = new boolean[25];
	public static boolean[] repetidos3 = new boolean[25];
	public static boolean[] repetidos4 = new boolean[25];
	public static boolean[] repetidos5 = new boolean[25];
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yonunca);
		
		//Cargamos las tipografias de los TextViews
		futuraHandwritten = Typeface.createFromAsset(getAssets(),"Fuentes/FuturaHandwritten.ttf");

		txtResultado = (TextView)findViewById(R.id.txtResultado);
		UsuariosSQLiteHelper usdbh = new UsuariosSQLiteHelper(this);
		try {
			usdbh.crearDataBase();
		} catch (IOException e) {}

 
        db = usdbh.getWritableDatabase();
        txtResultado.setText("");
        int valorRandom;
        int registros;
        int tipoAleatorio=0;
        String tipo = "";
        String frase = "";
        Cursor c;
       
        
        int exit=0;
        
        while(exit==0){
           
        	tipoAleatorio = (int) Math.floor(Math.random()*5+1);
        	
        	if(tipoAleatorio==1 && OpcionesActivity.botonyonunca==true|| 
        			tipoAleatorio==2 && OpcionesActivity.botonpalabrasprohibidas==true||
        			tipoAleatorio==3 && OpcionesActivity.botondesafio==true||
        			tipoAleatorio==4 && OpcionesActivity.botontodos==true||
        			tipoAleatorio==5 && OpcionesActivity.botonduelo==true){
        		
        		exit=1;
        		
        	}
        	
        }


 	   
        
	//****************************TABLA YO NUNCA**********************
       if(tipoAleatorio==1){
    	   		int i=0;
       	      	c = db.rawQuery("SELECT tipo, frase FROM YoNunca", null);
       	      	registros = c.getCount();
       	      	valorRandom = (int) Math.floor(Math.random()*registros);
       	      	//Buble que rellena la posicion del vector del Random con un 1 si no ha salido aun
       	      	while(ResultadoActivity.repetidos1[valorRandom]==true&&cont1!=24){
       	      		valorRandom = (int) Math.floor(Math.random()*registros);
       	      		}
       			
       	      	c.moveToPosition(valorRandom);
       	      	ResultadoActivity.repetidos1[valorRandom] = true;
       	      	if(cont1==registros-1)
       	      	{
       	      		for(i=0;i<=24; i++)
       	      		{
       	      			ResultadoActivity.repetidos1[i]= false;
       	      		}
       	     
       	      		System.out.println("array puesto a cero");
       	      		cont1=0;
       	      	}
       	      	ResultadoActivity.cont1 = ResultadoActivity.cont1+1;
       	      	tipo = c.getString(0);
       	      	frase = c.getString(1);
           
       	      	c.close();          
       	      	
       }
     
//**********************TABLA YO PALABRAS PROHIBIDAS******************
       if(tipoAleatorio==2){ 
    	   int i=0;
    	   setContentView(R.layout.palabrasprohibidas);
    	   
    	   txtResultado = (TextView)findViewById(R.id.txtResultado);
    		   	c = db.rawQuery("SELECT tipo, frase FROM PalabrasProhibidas", null);
    		   	registros = c.getCount();
    		   	valorRandom = (int) Math.floor(Math.random()*registros);
    		   	while(ResultadoActivity.repetidos2[valorRandom]==true&&cont2!=24){
       	      		valorRandom = (int) Math.floor(Math.random()*registros);
       	      		}
    		   	c.moveToPosition(valorRandom);
    		   	ResultadoActivity.repetidos2[valorRandom] = true;
       	      	if(cont2==registros-1)
       	      	{
       	      		for(i=0;i<=24; i++)
       	      		{
       	      			ResultadoActivity.repetidos2[i]= false;
       	      		}
       	      		System.out.println("array puesto a cero");
       	      		cont2=0;
       	      	}
       	      	ResultadoActivity.cont1 = ResultadoActivity.cont1+1;
    		   	tipo = c.getString(0);
    		   	frase = c.getString(1);
           
    		   	c.close();  
    		   	
       }	
       
//****************************TABLA YO DESAFIO**********************
       if(tipoAleatorio==3){ 
    	   int i=0;
    	   setContentView(R.layout.desafio);
    	   
    	   txtResultado = (TextView)findViewById(R.id.txtResultado);
    	   
    		   c = db.rawQuery("SELECT tipo, frase FROM Desafio", null);
    		   registros = c.getCount();
    		   valorRandom = (int) Math.floor(Math.random()*registros);
    			while(ResultadoActivity.repetidos3[valorRandom]==true&&cont3!=24){
       	      		valorRandom = (int) Math.floor(Math.random()*registros);
       	      		}
    		   c.moveToPosition(valorRandom);
    		   ResultadoActivity.repetidos3[valorRandom] = true;
      	      	if(cont3==registros-1)
      	      	{
      	      		for(i=0;i<=24; i++)
      	      		{
      	      			ResultadoActivity.repetidos3[i]= false;
      	      		}
      	      		System.out.println("array puesto a cero");
      	      		cont3=0;
      	      	}
      	      	ResultadoActivity.cont3 = ResultadoActivity.cont3+1;
    		   tipo = c.getString(0);
    		   frase = c.getString(1);
           
    		   c.close();
    		   
       }
       
//****************************TABLA YO TODOS**********************
       if(tipoAleatorio==4){
    	   int i=0;
    	   setContentView(R.layout.todos);
    	   
    	   txtResultado = (TextView)findViewById(R.id.txtResultado);
    	   
    	   		c = db.rawQuery("SELECT tipo, frase FROM Todos", null);
    	   		registros = c.getCount();
    	   		valorRandom = (int) Math.floor(Math.random()*registros);
    	   		while(ResultadoActivity.repetidos4[valorRandom]==true&&cont4!=24){
       	      		valorRandom = (int) Math.floor(Math.random()*registros);
       	      		}
    	   		c.moveToPosition(valorRandom);
    	   		ResultadoActivity.repetidos4[valorRandom] = true;
      	      	if(cont4==registros-1)
      	      	{
      	      		for(i=0;i<=24; i++)
      	      		{
      	      			ResultadoActivity.repetidos4[i]= false;
      	      		}
      	      		System.out.println("array puesto a cero");
      	      		cont4=0;
      	      	}
      	      	ResultadoActivity.cont4 = ResultadoActivity.cont4+1;
    	   		tipo = c.getString(0);
    	   		frase = c.getString(1);
    	   		
    	   		c.close();
    	   		
       }
       
//****************************TABLA YO DUELO**********************
       if(tipoAleatorio==5){ //TABLA DUELO
    	   int i=0;
    	   setContentView(R.layout.duelo);
    	   
    	   txtResultado = (TextView)findViewById(R.id.txtResultado);
    	   
    		   c = db.rawQuery("SELECT tipo, frase FROM Duelo", null);
    		   registros = c.getCount();
    		   valorRandom = (int) Math.floor(Math.random()*registros);
    		   while(ResultadoActivity.repetidos5[valorRandom]==true&&cont5!=24){
     	      		valorRandom = (int) Math.floor(Math.random()*registros);
     	      		}
    		   c.moveToPosition(valorRandom);
    		   ResultadoActivity.repetidos5[valorRandom] = true;
     	      	if(cont5==registros-1)
     	      	{
     	      		for(i=0;i<=24; i++)
     	      		{
     	      			ResultadoActivity.repetidos5[i]= false;
     	      		}
     	      		System.out.println("array puesto a cero");
     	      		cont5=0;
     	      	}
     	      	ResultadoActivity.cont5 = ResultadoActivity.cont5+1;
           
    		   tipo = c.getString(0);
    		   frase = c.getString(1);
           
    		   c.close();
    		   
       } 
       
       txtResultado.setTypeface(futuraHandwritten);
       txtResultado.append(" " + tipo + " - " + frase + "\n");
       
       db.close();
       
	}
	

	public void volveraDorso(View v){
		MediaPlayer mp = MediaPlayer.create(this, R.raw.repartiendo);
        mp.start();
		
		Intent Volver = new Intent(this, JugarActivity.class);
		startActivity(Volver);
		this.finish();
	}
	

	@Override
	public void onBackPressed() {
		
		super.onBackPressed();
		Intent Volver = new Intent(this, JugarActivity.class);
		startActivity(Volver);
		this.finish();
	}
		
}
