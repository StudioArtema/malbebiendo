package com.example.malbebiendo;

import android.app.Activity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.TextView;
import android.widget.ToggleButton;


public class OpcionesActivity extends Activity {
	public static boolean botonyonunca = true;
	public static boolean botonpalabrasprohibidas = true;
	public static boolean botondesafio = true;
	public static boolean botonduelo = true;
	public static boolean botontodos = true;
	public static ToggleButton tglbtn1;
	public static ToggleButton tglbtn2;
	public static ToggleButton tglbtn3;
	public static ToggleButton tglbtn4;
	public static ToggleButton tglbtn5;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_opciones);
		TextView tv = (TextView)findViewById(R.id.textView1);
		Linkify.addLinks(tv, Linkify.ALL);
		
		
	    tglbtn1=(ToggleButton) findViewById(R.id.botonYoNunca);
	    tglbtn1.setChecked(botonyonunca);
	    
	    tglbtn2=(ToggleButton) findViewById(R.id.botonPalabrasProhibidas);
	    tglbtn2.setChecked(botonpalabrasprohibidas);

	    tglbtn3=(ToggleButton) findViewById(R.id.botonDesafio);
	    tglbtn3.setChecked(botondesafio);

	    tglbtn4=(ToggleButton) findViewById(R.id.botonDuelo);
	    tglbtn4.setChecked(botonduelo);

	    tglbtn5=(ToggleButton) findViewById(R.id.botonTodos);
	    tglbtn5.setChecked(botontodos);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		//botones intermedios para saber el estado final
		final boolean botonyonuncaint;
		final boolean botonpalabrasprohibidasint;
		final boolean botondesafioint;
		final boolean botondueloint;
		final boolean botontodosint;
		 if (tglbtn1.isChecked())
				botonyonuncaint = true;
			else
				botonyonuncaint = false;
		botonyonunca = botonyonuncaint;
		if (tglbtn2.isChecked())
			botonpalabrasprohibidasint = true;
		else
			botonpalabrasprohibidasint = false;
    	botonpalabrasprohibidas = botonpalabrasprohibidasint;
    	if (tglbtn3.isChecked())
			botondesafioint = true;
		else
			botondesafioint = false;
    	botondesafio = botondesafioint;
    	if (tglbtn4.isChecked())
			botondueloint = true;
		else
			botondueloint = false;
    	botonduelo = botondueloint;
    	if (tglbtn5.isChecked())
			botontodosint = true;
		else
			botontodosint = false;
    	botontodos = botontodosint;
    	
	}
	
}