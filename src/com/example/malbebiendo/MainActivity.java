package com.example.malbebiendo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.content.DialogInterface;
import android.content.Intent;


public class MainActivity extends Activity {

	private static final int MNU_OPC1 = 1;
	Handler myHandler = new Handler();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		menu.add(Menu.NONE, MNU_OPC1, Menu.NONE, "Acerca de")
		.setIcon(android.R.drawable.ic_menu_preferences);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        
	    	case MNU_OPC1:
	    		Acercade acercade = new Acercade(this);
    			acercade.setTitle("Acerca de");
    			acercade.show();
	        return true;
	       
	    	default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	//Este es el método que actua cuando pulsamos el boton Jugar
	public void iraJugar(View v){
		MediaPlayer mp = MediaPlayer.create(this, R.raw.barajando);
        mp.start();
		Intent Jugar = new Intent(this, JugarActivity.class);
		startActivity(Jugar);

	}
	
	//Este es el método que actua cuando pulsamos el boton Opciones
	//De momento no operativo (hay que hacer un OpcionesActivity.java
	//y el xml
	public void iraOpciones(View view){
		Intent Opciones = new Intent(this, OpcionesActivity.class);
		startActivity(Opciones);
	}
	
	public void iraInstruciones(View view){
		Intent Instrucciones = new Intent(this, InstruccionesActivity.class);
		startActivity(Instrucciones);
	}
	


	@Override
	public void onBackPressed() {
	    new AlertDialog.Builder(this)
	        .setIcon(R.drawable.ic_launcher)
	        .setTitle("Que haces cerrando Malbebiendo?")
	        .setMessage("¿Seguro que quieres dejar de malbeber?")
	        .setPositiveButton("Si, demasiado pal' body", new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	        	Log.i("HA", "Finishing");
	        	Intent intent = new Intent(Intent.ACTION_MAIN);
	        	intent.addCategory(Intent.CATEGORY_HOME);
	        	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        	startActivity(intent);
	        }

	    })
	    .setNegativeButton("No, otra ronda", null)
	    .show();
	}

	
	
}
  