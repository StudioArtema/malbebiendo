package com.example.malbebiendo;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

public class JugarActivity extends Activity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jugar);
	}
	
	@Override
    public void onBackPressed() {
            super.onBackPressed();
            Intent Volver = new Intent(this, MainActivity.class);
    		startActivity(Volver);
    		this.finish();
    }
	
	public void iraResultado(View v){	
		MediaPlayer mp = MediaPlayer.create(this, R.raw.repartiendo);
        mp.start();
		Intent Resultado = new Intent(this, ResultadoActivity.class);
		startActivity(Resultado);
		this.finish();
		overridePendingTransition(R.anim.rotacion,R.anim.rotacion2);
		
	}

}
